import React, {useState, useEffect, useContext} from 'react';
import {Container, Form, Button} from 'react-bootstrap';
import {Redirect} from 'react-router-dom';
import UserContext from './../UserContext';

export default function Register(){

	const[email, setEmail] = useState('');
	const[password, setPassword] = useState('');
	const[verifyPassword, setVerifyPassword] = useState('');
	const[isDisabled, setIsDisabled] = useState(true);

	const {setUser} = useContext(UserContext);

	// useEffect();
	useEffect (() => {
		if(email !== "" && password !== "" && verifyPassword !== "" && password === verifyPassword){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [email, password, verifyPassword]);

	function register (e){
		e.preventDefault();
		alert('Registration Successful, you may now login');
		localStorage.setItem('email', email, 'password', password, 'verifyPassword', verifyPassword)
		


		// setEmail('');
		// setPassword('');
		setEmail({email: email});
		setPassword({password: password});
		setVerifyPassword({verifyPassword: verifyPassword});

		setEmail('');
		setPassword('');
		setVerifyPassword('');
	}

	if (email !== null){
		return <Redirect to="/login"/>
	}
	return(
		<Container className="mb-5">
			<h1 className="text-center">Register</h1>
			<Form onSubmit={register}>
			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e)=> setEmail(e.target.value)}/>
			  </Form.Group> 
			  <Form.Group className="mb-3" controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control type="password" placeholder="Password" value={password} onChange={(e)=> setPassword(e.target.value)}/>
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="formVerifyPassword">
			    <Form.Label>Verify Password</Form.Label>
			    <Form.Control type="password" placeholder="Verify Password" value={verifyPassword} onChange={(e)=>setVerifyPassword(e.target.value)}/>
			  </Form.Group>
			  <Button variant="primary" type="submit" disabled={isDisabled}>
			    Submit
			  </Button>
			</Form>
		</Container>
		)
}