import React, {Fragment, useContext} from 'react';
import {Link, NavLink, useHistory} from 'react-router-dom';
import UserContext from './../UserContext';
// import Register from './../pages/Register';

import {Navbar, Nav} from 'react-bootstrap';
// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';

export default function AppNavbar(){

  const {user, unsetUser} = useContext(UserContext)
 
  let history = useHistory();

  const logout = () => {
    unsetUser();
    history.push('/login')
  }

  let leftNav = (user.email ===null)? (
    <Fragment>
      <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
      <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
    </Fragment>
    ): (<Fragment>
          <Nav.Link onClick={logout}>Logout</Nav.Link>
        </Fragment>
    )


  return (
    <Navbar bg="" expand="lg">
      <Navbar.Brand as={Link} to="/">Course Booking</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav>
          <Nav.Link as={NavLink} to="/">Home</Nav.Link>
          <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
        </Nav>
        <Nav>
          {leftNav}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
    )
}