import React from 'react';
import {
	Container,
	Row,
	Col,
	Jumbotron,
	Button
} from 'react-bootstrap';

export default function Banner (){
	return (
		// <div className="container-fluid">
		// 	<div className="row justify-content-center">
		// 		<div className="col-10 col-md-8">
		// 			<div className="jumbotron">
		// 				<h1>Zuitt Coding Bootcamp</h1>
		// 				<p1>Opportunities for everyone, everywhere</p1>
		// 				<button className=btn btn-primary>Enroll</button>
		// 			</div>
		// 		</div>
		// 	</div>
		// </div>

	<Container fluid>
		<Row>
			<Col className="px-0">
				<Jumbotron fluid className="px-3">
				  <h1>Hello, world!</h1>
				  <p>
				    This is a simple hero unit, a simple jumbotron-style component for calling
				    extra attention to featured content or information.
				  </p>
				  <p>
				    <Button variant="primary">Learn more</Button>
				  </p>
				</Jumbotron>
			</Col>
		</Row>
	</Container>
	)
}